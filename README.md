# README #

Catalogo de peliculas y series de IMDB

### Para que se usa este repositorio? ###

* Este repositorio es usado para mostrar el desarrollo de una pagina web basica con fines educativos, hecha con Javascript + Bootstrap, usando una API de pruebas de IMDB, para poder mostrar un catalogo de peliculas.

### Que necesito para que funcione? ###

* Un editor de codigo como Visual Studio Code
* Un navegador web
* Generarse una API key en la siguiente web: http://www.omdbapi.com/apikey.aspx
* Generar un archivo llamado .env, para poder agregar la api key

### Webs de ayuda ###

* Javascript: https://developer.mozilla.org/es/docs/Web/JavaScript
* Bootstrap: https://getbootstrap.com/
* Omdb API: http://www.omdbapi.com/

### Creador ###

* Victor Di Lena: https://www.linkedin.com/in/victor-di-lena-50263121/