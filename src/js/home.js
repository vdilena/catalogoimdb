const cargarNombreUsuarioLogueado = () => {

    const nombre = localStorage.getItem("usuarioLogueado")
    document.getElementById("usuarioLogueado").innerText = nombre
}

const cerrarSesion = () => {
    localStorage.removeItem("usuarioLogueado")
    redirigirALogin()
}

const redirigirALogin = () => {
    location.href = "login.html"
}

cargarNombreUsuarioLogueado()