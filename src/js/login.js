const ejecutarValidaciones = () => {

    let esValido = false
    const nombreUsuario = document.getElementById("usuario").value
    const password = document.getElementById("password").value

    esValido = esValidoNombreUsuario(nombreUsuario) && esValidoPassword(password)

    if( esValido ) {

        quitarMensajeDeError("usuario")
        cargarUsuarioLogueado(nombreUsuario)
        redirigirAHome()
    } else {
        agregarMensajeDeError("usuario", "El nombre de usuario y/o la contraseña son invalidos")
        agregarMensajeDeError("password")
    }
}

const esValidoPassword = (password) => {

    let esValido = true

    if( !esPasswordValido(password, 8)) {
        esValido = false
    } 

    return esValido
}

const esValidoNombreUsuario = (nombreUsuario, password) =>  {

    let esValido = true
    
    if( !esNombreUsuarioValido(nombreUsuario, 8)  ) {
        esValido = false
    } else {
        const usuarioExistente = obtenerUsuarioExistente(nombreUsuario)

        if(!usuarioExistente) {
            esValido = false
        }
    }

    return esValido
}

const obtenerUsuarioExistente = (username) => {

    let usuarios = JSON.parse(localStorage.getItem("usuarios"))
    const mensajeUsuarioOPasswordInvalidos = "El nombre de usuario o la contraseña son invalidos"

    if(!usuarios || usuarios.length === 0) {
        agregarMensajeDeError("usuario", mensajeUsuarioOPasswordInvalidos)
        return false
    } else {

        let usuarioEncontrado = obtenerUsuario(usuarios, username)

        if(!usuarioEncontrado) {
            agregarMensajeDeError("usuario", mensajeUsuarioOPasswordInvalidos)
            return false
        }

        return usuarioEncontrado
    }
}

const obtenerUsuario = (usuarios, username) => {

    let usuariosEncontrados = usuarios.filter(usuario => usuario.username === username)

    if(usuariosEncontrados.length > 0) {
        let usuario = usuariosEncontrados.pop()
        return usuario
    }
    return null
}