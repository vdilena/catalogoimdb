
const redirigirAHome = () => {
    location.href = "index.html"
}

const esNombreUsuarioValido = (nombreUsuario, cantidadMinimaCaracteres) => {

    if( !cumpleMinimoLongitud(nombreUsuario, cantidadMinimaCaracteres)  ) {
        return false
    }

    return true
}

const esPasswordValido = (password, cantidadMinimaCaracteres) => {

    if( !cumpleMinimoLongitud(password, cantidadMinimaCaracteres) 
        || !tieneAlMenosTresNumeros(password) 
        || !tieneAlMenosTresLetras(password)
    ) {
        return false
    }

    return true

}

const cumpleMinimoLongitud = (palabra, cantidadCaracteresMinimo) => {

    if(palabra.length < cantidadCaracteresMinimo) {
        return false
    }

    return true
}

const tieneAlMenosTresNumeros = (palabra) => {

    let cantidadDeNumeros = 0
    for(let i = 0 ; i < palabra.length; i++) {

        if( !isNaN(palabra[i]) ) {
            cantidadDeNumeros++
        }
    }

    return cantidadDeNumeros >= 3
}

const tieneAlMenosTresLetras = (palabra) => {

    let cantidadDeLetras = 0
    for(let i = 0 ; i < palabra.length; i++) {

        if( isNaN(palabra[i]) ) {
            cantidadDeLetras++
        }
    }

    return cantidadDeLetras >= 3
}

const agregarMensajeDeError = (idElementoDOM, mensaje) => {

    document.getElementById(idElementoDOM).classList.remove("is-valid")
    
    document.getElementById(idElementoDOM).classList.add("is-invalid")

    if(mensaje) {
        document.getElementById("error_" + idElementoDOM + "_incorrecto").innerText = mensaje
    }
}

const quitarMensajeDeError = (idElementoDOM) => {

    document.getElementById(idElementoDOM).classList.remove("is-invalid")

    document.getElementById(idElementoDOM).classList.add("is-valid")
    document.getElementById("error_" + idElementoDOM + "_incorrecto").innerText = ""
}

const cargarUsuarioLogueado = (nombre) => {
    localStorage.setItem("usuarioLogueado", nombre)
}