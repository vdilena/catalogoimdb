
const ejecutarValidaciones = () => {

    let esValido = false
    const nombreUsuario = document.getElementById("usuario").value
    const nombre = document.getElementById("nombre").value
    const email = document.getElementById("email").value
    const password = document.getElementById("password").value

    esValido = esValidoNombreUsuario(nombreUsuario)
    esValido = validarEmail(email)
    esValido = validarPassword(password)

    if( esValido ) {

        const nuevoUsuario = new Usuario(nombreUsuario, nombre, email, password)
        guardarUsuario(nuevoUsuario)
        cargarUsuarioLogueado(nombre)
        redirigirAHome()
    }
}

const validarEmail = (email) => {

    let esValido = true

    if(!contieneArroba(email) || !contienePuntoCom(email)) {

        agregarMensajeDeError("email", "El email debe tener un formato valido (con @ y .com)")
        esValido = false
    } else {
        quitarMensajeDeError("email")
    }

    return esValido
}

const validarConfirmacionPassword = (password, confirmPassword) => {

    let esValido = true

    if(confirmPassword != password) {

        agregarMensajeDeError("confirmPassword", "Las contraseñas ingresadas, deben coincidir")
        esValido = false
    } else {
        quitarMensajeDeError("confirmPassword")
    }

    return esValido
}

const validarNombreUsuario = (nombreUsuario) =>  {

    let esValido = true
    
    if( !esNombreUsuarioValido(nombreUsuario, 8)  ) {

        agregarMensajeDeError("usuario", "El nombre de usuario debe tener al menos 8 caracteres")
        esValido = false
    } else {
        quitarMensajeDeError("usuario")
        esValido = validarEsUsuarioInexistente(nombreUsuario)
    }

    return esValido
}

const validarEsUsuarioInexistente = (nombreUsuario) => {

    let esValido = true

    if (existeUsuarioEnBaseDeDatos(nombreUsuario)) {

        agregarMensajeDeError("usuario", "El usuario que intenta ingresar, ya existe en el sistema")
        esValido = false
    } else {
        quitarMensajeDeError("usuario")
    }

    return esValido
}

const validarPassword = (password) => {

    let esValido = true

    if( !esPasswordValido(password, 8)) {

        agregarMensajeDeError("password", "La contraseña debe tener minimo 8 caracteres y al menos 3 numeros y 3 letras")
        esValido = false
    } else {
        
        quitarMensajeDeError("password")
        esValido = validarConfirmacionPassword(password, document.getElementById("confirmPassword").value)
    }

    return esValido
}

const contieneArroba = (email) => {
    return contieneSubPalabra(email, "@")
}

const contienePuntoCom = (email) => {
    return contieneSubPalabra(email, ".com")
}

const contieneSubPalabra = (palabra, subPalabra) => {

    if(palabra.includes(subPalabra)) {
        return true
    }

    return false
}

const existeUsuarioEnBaseDeDatos = (nombreUsuario) => {

    const usuarios = obtenerElementoDeBaseDeDatos("usuarios")
    const existeUsuario = usuarios.filter(usuario => usuario.nombre === nombreUsuario).length > 0

    return existeUsuario
}

const obtenerElementoDeBaseDeDatos = (elemento) => {

    let elementoDeBaseDeDatos = localStorage.getItem(elemento)

    if(!elementoDeBaseDeDatos) {
        elementoDeBaseDeDatos = crearYObtenerElementoListaDeBaseDeDatos(elemento) 
    }

    return JSON.parse(elementoDeBaseDeDatos)
}

const crearYObtenerElementoListaDeBaseDeDatos = (elementoLista) => {
    
    localStorage.setItem(elementoLista.toString(), JSON.stringify([]))
    return localStorage.getItem(elementoLista)
}

const guardarUsuario = (usuario) => {

    let listaUsuarios = obtenerElementoDeBaseDeDatos("usuarios")
    listaUsuarios.push(usuario)
    localStorage.setItem("usuarios", JSON.stringify(listaUsuarios))
}